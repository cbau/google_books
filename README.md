# Google Books

Search books using Google Book's API.

## Screenshots

<img src="./screenshots/book-list-small.png" alt="Book list on small screens screenshot" width="240" /> <img src="./screenshots/book-details-small.png" alt="Book details on small screens screenshot" width="240" />
<img src="./screenshots/book-list-big.png" alt="Book list on big screens screenshot" width="480" /> <img src="./screenshots/book-details-big.png" alt="Book details on big screens screenshot" width="480" />

## Features

- The app is created as a Google Book's API consumption showcase.
- Search any word in the API through the search at the app bar.
- The search app bar is designed to look and work similar to Android's native `SearchView`.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/google_books/)

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```

For web add the option `--web-renderer html`, as canvaskit doesn't support images served with CORS disabled:

```bash
flutter run --release --web-renderer html
```
