import 'package:http/http.dart';

class GoogleBookService {
  /// Factory instance constructor.
  factory GoogleBookService() => _instance;

  /// Private constructor for this class.
  const GoogleBookService._();

  static const _instance = GoogleBookService._();

  final serviceOrigin = 'https://www.googleapis.com/books';

  Future<Response> searchBooksFromQuery(
    String query, {
    int startIndex = 0,
    int maxResults = 30,
  }) =>
      get(Uri.parse(
          '$serviceOrigin/v1/volumes?q=$query&startIndex=$startIndex&maxResults=$maxResults'));

  Future<Response> getVolumeForId(String volumeId) =>
      get(Uri.parse('$serviceOrigin/v1/volumes/$volumeId'));
}
