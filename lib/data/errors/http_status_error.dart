class HttpStatusError extends Error {
  HttpStatusError(
    this.statusCode, {
    this.reasonPhrase,
  });

  final int statusCode;
  final String? reasonPhrase;

  @override
  String toString() => 'HTTP Status Exception: $statusCode $reasonPhrase';
}
