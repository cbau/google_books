import 'volume_info.dart';

class Volume {
  Volume({
    required this.etag,
    required this.id,
    required this.selfLink,
    required this.volumeInfo,
  });

  Volume.empty()
      : this(
          etag: '',
          id: '',
          selfLink: '',
          volumeInfo: VolumeInfo.empty(),
        );

  Volume.fromMap(Map<String, Object?>? map)
      : this(
          etag: map?['etag'] as String? ?? '',
          id: map?['id'] as String? ?? '',
          selfLink: map?['selfLink'] as String? ?? '',
          volumeInfo:
              VolumeInfo.fromMap(map?['volumeInfo'] as Map<String, Object?>?),
        );

  final String etag;
  final String id;
  final String selfLink;
  final VolumeInfo volumeInfo;

  Map<String, Object?> toMap() {
    return {
      'etag': etag,
      'id': id,
      'selfLink': selfLink,
      'volumeInfo': volumeInfo.toMap(),
    };
  }
}
