import 'image_links.dart';

class VolumeInfo {
  VolumeInfo({
    required this.allowAnonLogging,
    required this.authors,
    required this.categories,
    required this.canonicalVolumeLink,
    required this.contentVersion,
    required this.description,
    required this.imageLinks,
    required this.infoLink,
    required this.language,
    required this.maturityRating,
    required this.pageCount,
    required this.previewLink,
    required this.printType,
    required this.publishedDate,
    required this.publisher,
    required this.title,
  });

  VolumeInfo.empty()
      : this(
          allowAnonLogging: false,
          authors: [],
          categories: [],
          canonicalVolumeLink: '',
          contentVersion: '',
          description: '',
          imageLinks: ImageLinks.empty(),
          infoLink: '',
          language: '',
          maturityRating: '',
          pageCount: 0,
          previewLink: '',
          printType: '',
          publishedDate: '',
          publisher: '',
          title: '',
        );

  VolumeInfo.fromMap(Map<String, dynamic>? map)
      : this(
          allowAnonLogging: map?['allowAnonLogging'] as bool? ?? false,
          authors: (map?['authors'] as List?)?.cast() ?? [],
          categories: (map?['categories'] as List?)?.cast() ?? [],
          canonicalVolumeLink: map?['canonicalVolumeLink'] as String? ?? '',
          contentVersion: map?['contentVersion'] as String? ?? '',
          description: map?['description'] as String? ?? '',
          imageLinks:
              ImageLinks.fromMap(map?['imageLinks'] as Map<String, dynamic>?),
          infoLink: map?['infoLink'] as String? ?? '',
          language: map?['language'] as String? ?? '',
          maturityRating: map?['maturityRating'] as String? ?? '',
          pageCount: map?['pageCount'] as int? ?? 0,
          previewLink: map?['previewLink'] as String? ?? '',
          printType: map?['printType'] as String? ?? '',
          publishedDate: map?['publishedDate'] as String? ?? '',
          publisher: map?['publisher'] as String? ?? '',
          title: map?['title'] as String? ?? '',
        );

  final bool allowAnonLogging;
  final List<String> authors;
  final List<String> categories;
  final String canonicalVolumeLink;
  final String contentVersion;
  final String description;
  final ImageLinks imageLinks;
  final String infoLink;
  final String language;
  final String maturityRating;
  final int pageCount;
  final String previewLink;
  final String printType;
  final String publishedDate;
  final String publisher;
  final String title;

  String get author {
    return authors.join(', ');
  }

  Map<String, Object?> toMap() {
    return {
      'allowAnonLogging': allowAnonLogging,
      'authors': authors,
      'categories': categories,
      'canonicalVolumeLink': canonicalVolumeLink,
      'contentVersion': contentVersion,
      'description': description,
      'imageLinks': imageLinks.toMap(),
      'infoLink': infoLink,
      'language': language,
      'maturityRating': maturityRating,
      'pageCount': pageCount,
      'previewLink': previewLink,
      'printType': printType,
      'publishedDate': publishedDate,
      'publisher': publisher,
      'title': title,
    };
  }
}
