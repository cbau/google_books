class ImageLinks {
  ImageLinks({
    required this.smallThumbnail,
    required this.thumbnail,
  });

  ImageLinks.empty()
      : this(
          smallThumbnail: '',
          thumbnail: '',
        );

  ImageLinks.fromMap(Map<String, dynamic>? map)
      : this(
          smallThumbnail: map?['smallThumbnail'] as String? ?? '',
          thumbnail: map?['thumbnail'] as String? ?? '',
        );

  final String smallThumbnail;
  final String thumbnail;

  Map<String, Object?> toMap() {
    return {
      'smallThumbnail': smallThumbnail,
      'thumbnail': thumbnail,
    };
  }
}
