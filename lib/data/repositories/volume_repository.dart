import 'dart:convert';

import '../errors/http_status_error.dart';
import '../models/volume.dart';
import '../services/google_books_service.dart';

class VolumeRepository {
  /// Factory instance constructor.
  factory VolumeRepository() => _instance;

  /// Private constructor for this class.
  const VolumeRepository._();

  static const _instance = VolumeRepository._();

  Future<List<Volume>> search(
    String query, {
    int page = 0,
  }) async {
    final response = await GoogleBookService().searchBooksFromQuery(
      query,
      startIndex: page * 30,
    );

    if (response.statusCode >= 300) {
      throw HttpStatusError(
        response.statusCode,
        reasonPhrase: response.reasonPhrase,
      );
    }

    final map = json.decode(response.body) as Map<String, dynamic>;
    return _JsonContainer._fromMap(map).items;
  }

  Future<Volume> get(String id) async {
    final response = await GoogleBookService().getVolumeForId(id);

    if (response.statusCode >= 300) {
      throw HttpStatusError(
        response.statusCode,
        reasonPhrase: response.reasonPhrase,
      );
    }

    final map = json.decode(response.body) as Map<String, dynamic>;
    return Volume.fromMap(map);
  }
}

class _JsonContainer {
  const _JsonContainer({
    required this.items,
    required this.totalItems,
  });

  _JsonContainer._fromMap(Map<String, dynamic> map)
      : this(
          items: (map['items'] as List?)
                  ?.map((e) => Volume.fromMap(e as Map<String, dynamic>))
                  .toList() ??
              [],
          totalItems: map['totalItems'] as int? ?? 0,
        );

  final List<Volume> items;
  final int totalItems;
}
