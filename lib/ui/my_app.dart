import 'package:flutter/material.dart';

import 'app_router.dart';
import 'app_theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //FirebaseCrashlytics.instance.crash();

    return MaterialApp.router(
      darkTheme: AppTheme().dark,
      routerConfig: AppRouter().router,
      theme: AppTheme().light,
      //themeMode: ThemeMode.dark,
      title: 'Google Books',
    );
  }
}
