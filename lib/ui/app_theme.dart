import 'package:flutter/material.dart';

class AppTheme {
  /// Factory instance constructor.
  factory AppTheme() => _instance;

  /// Private constructor for this class.
  AppTheme._();

  static final AppTheme _instance = AppTheme._();

  ThemeData? _dark;
  ThemeData? _light;

  ThemeData get dark => _dark ??= _build(isLight: false);

  ThemeData get light => _light ??= _build(isLight: true);

  ThemeData _build({required bool isLight}) {
    return ThemeData(
      brightness: isLight ? Brightness.light : Brightness.dark,
      primarySwatch: Colors.blue,
    );
  }
}
