import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../data/models/volume.dart';
import 'book_details/book_details_page.dart';
import 'book_list/book_list_page.dart';
import 'generic/not_found_page.dart';

class AppRouter {
  /// Factory instance constructor.
  factory AppRouter() => _instance;

  /// Private constructor for this class.
  AppRouter._();

  static final AppRouter _instance = AppRouter._();

  final _navigationPageKey = const ValueKey('navigationPage');

  late final router = GoRouter(
    errorBuilder: (context, state) => const NotFoundPage(),
    routes: [
      GoRoute(
        path: '/books',
        redirect: (context, state) => '/',
      ),
      GoRoute(
        name: BookDetailsPage.routeName,
        pageBuilder: (context, state) => MaterialPage<void>(
          child: BookDetailsPage(
            itemId: state.params['id'] ?? '',
            item: state.extra as Volume?,
          ),
        ),
        path: '/books/:id',
      ),
      GoRoute(
        name: BookListPage.routeName,
        pageBuilder: (context, state) => MaterialPage<void>(
          key: _navigationPageKey,
          child: const BookListPage(),
        ),
        path: '/',
      ),
    ],
  );
}
