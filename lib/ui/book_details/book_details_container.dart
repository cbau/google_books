import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

import '../../data/models/volume.dart';
import 'book_details_content.dart';

class BookDetailsContainer extends StatelessWidget {
  const BookDetailsContainer({
    required this.item,
    Key? key,
  }) : super(key: key);

  final Volume item;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          item.volumeInfo.title,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      body: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconButton(
                  icon: const Icon(Icons.share),
                  onPressed: () => Share.share(item.selfLink),
                  tooltip: 'Share',
                ),
                SizedBox(
                  width: 240,
                  child: item.volumeInfo.imageLinks.smallThumbnail.isEmpty
                      ? null
                      : Hero(
                          tag: 'thumbnail${item.id}',
                          child: CachedNetworkImage(
                            fit: BoxFit.cover,
                            imageUrl: item.volumeInfo.imageLinks.smallThumbnail,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                )
              ],
            ),
          ),
          Expanded(
            child: BookDetailsContent(item: item),
          ),
        ],
      ),
    );
  }
}
