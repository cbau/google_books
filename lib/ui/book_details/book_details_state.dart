import '../../data/models/volume.dart';

class BookDetailsState {
  const BookDetailsState({
    this.isLoading = false,
    this.volume,
  });

  final bool isLoading;
  final Volume? volume;

  BookDetailsState copyWith({
    bool? isLoading,
    Volume? volume,
  }) {
    return BookDetailsState(
      isLoading: isLoading ?? this.isLoading,
      volume: volume ?? this.volume,
    );
  }
}
