import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:share_plus/share_plus.dart';

import '../../data/models/volume.dart';
import 'book_details_content.dart';

class BookDetailsContainerSmall extends StatefulWidget {
  const BookDetailsContainerSmall({
    required this.item,
    Key? key,
  }) : super(key: key);

  final Volume item;

  @override
  State<BookDetailsContainerSmall> createState() =>
      _BookDetailsContainerSmallState();
}

class _BookDetailsContainerSmallState extends State<BookDetailsContainerSmall>
    with TickerProviderStateMixin<BookDetailsContainerSmall> {
  late final _fabAnimation = AnimationController(
    duration: kThemeAnimationDuration,
    value: 1,
    vsync: this,
  );

  late final ScrollController _scrollController = ScrollController(
    initialScrollOffset: 300,
  )..addListener(() {
      if (_scrollController.position.userScrollDirection !=
              ScrollDirection.idle &&
          _scrollController.position.maxScrollExtent !=
              _scrollController.position.minScrollExtent) {
        _scrollController.position.userScrollDirection ==
                ScrollDirection.forward
            ? _fabAnimation.forward()
            : _fabAnimation.reverse();
      }
    });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (context, innerBoxIsScrolled) {
          return [
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverAppBar(
                expandedHeight:
                    widget.item.volumeInfo.imageLinks.smallThumbnail.isEmpty
                        ? null
                        : 480,
                flexibleSpace: FlexibleSpaceBar(
                  background:
                      widget.item.volumeInfo.imageLinks.smallThumbnail.isEmpty
                          ? null
                          : Hero(
                              tag: 'thumbnail${widget.item.id}',
                              child: CachedNetworkImage(
                                color: Theme.of(context).primaryColor,
                                colorBlendMode: BlendMode.multiply,
                                fit: BoxFit.cover,
                                imageUrl: widget
                                    .item.volumeInfo.imageLinks.smallThumbnail,
                                width: MediaQuery.of(context).size.width,
                              ),
                            ),
                  title: Text(
                    widget.item.volumeInfo.title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                forceElevated: innerBoxIsScrolled,
                pinned: true,
              ),
            ),
          ];
        },
        body: Builder(
          builder: (context) => BookDetailsContent(
            item: widget.item,
            useOverlapInjector: true,
          ),
        ),
      ),
      floatingActionButton: ScaleTransition(
        scale: _fabAnimation,
        child: FloatingActionButton(
          onPressed: () => Share.share(widget.item.selfLink),
          tooltip: 'Share',
          child: const Icon(Icons.share),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _fabAnimation.dispose();
    _scrollController.dispose();
    super.dispose();
  }
}
