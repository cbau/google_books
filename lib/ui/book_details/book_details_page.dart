import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/models/volume.dart';
import '../generic/not_found_page.dart';
import 'book_details_container.dart';
import 'book_details_container_small.dart';
import 'book_details_cubit.dart';
import 'book_details_state.dart';

class BookDetailsPage extends StatelessWidget {
  const BookDetailsPage({
    required this.itemId,
    this.item,
    Key? key,
  }) : super(key: key);

  static const String routeName = 'BookDetails';

  final String itemId;
  final Volume? item;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BookDetailsCubit(
        volumeId: itemId,
        volume: item,
      ),
      child: const _ProvidedPage(),
    );
  }
}

class _ProvidedPage extends StatelessWidget {
  const _ProvidedPage();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BookDetailsCubit, BookDetailsState>(
        builder: (context, state) {
      return state.isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : state.volume == null
              ? const NotFoundPage()
              : LayoutBuilder(builder: (context, constraints) {
                  return constraints.minWidth < 600
                      ? BookDetailsContainerSmall(item: state.volume!)
                      : BookDetailsContainer(item: state.volume!);
                });
    });
  }
}
