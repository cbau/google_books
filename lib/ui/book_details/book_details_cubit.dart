import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/models/volume.dart';
import '../../data/repositories/volume_repository.dart';
import 'book_details_state.dart';

class BookDetailsCubit extends Cubit<BookDetailsState> {
  BookDetailsCubit({
    required String volumeId,
    Volume? volume,
  }) : super(BookDetailsState(
          volume: volume,
        )) {
    if (volume == null) {
      _getVolume(id: volumeId);
    }
  }

  void _getVolume({required String id}) async {
    emit(state.copyWith(
      isLoading: true,
    ));
    try {
      final volume = await VolumeRepository().get(id);
      emit(state.copyWith(
        isLoading: false,
        volume: volume,
      ));
    } catch (e, s) {
      debugPrint('$e\n$s');
      emit(state.copyWith(
        isLoading: false,
      ));
    }
  }
}
