import 'package:flutter/material.dart';

import '../../data/models/volume.dart';

class BookDetailsContent extends StatelessWidget {
  const BookDetailsContent({
    required this.item,
    this.useOverlapInjector = false,
    Key? key,
  }) : super(key: key);

  final Volume item;
  final bool useOverlapInjector;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        if (useOverlapInjector)
          SliverOverlapInjector(
            handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
          ),
        SliverPadding(
          padding: const EdgeInsets.all(16),
          sliver: SliverList(
            delegate: SliverChildListDelegate(
              [
                Text(
                  item.volumeInfo.title,
                  style: Theme.of(context).textTheme.headline6,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  item.volumeInfo.author,
                  style: Theme.of(context).textTheme.caption,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  item.volumeInfo.description,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
                /*
                RichText(
                  text: HTML.toTextSpan(
                    context,
                    item.volumeInfo.description,
                    defaultTextStyle:
                        Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                */
              ],
            ),
          ),
        ),
      ],
    );
  }
}
