import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/models/volume.dart';
import '../../data/repositories/volume_repository.dart';
import 'book_list_state.dart';

class BookListCubit extends Cubit<BookListState> {
  BookListCubit() : super(const BookListState());

  final _items = <Volume>[];

  Future<void> search(
    String query, {
    int page = 0,
  }) async {
    if (query.isEmpty) {
      _items.clear();
      emit(state.copyWith(
        errorMessage: '',
        items: _items,
        query: query,
      ));
      return;
    }

    emit(state.copyWith(
      errorMessage: '',
      isLoading: true,
    ));

    try {
      final items = await VolumeRepository().search(query, page: page);
      if (page == 0) {
        _items.clear();
      }
      _items.addAll(items);
      debugPrint('Item count: ${_items.length}');

      emit(state.copyWith(
        isLoading: false,
        items: _items,
        query: query,
      ));
    } catch (e) {
      emit(state.copyWith(
        errorMessage: e.toString(),
        isLoading: false,
      ));
    }
  }
}
