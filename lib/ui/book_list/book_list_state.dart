import '../../data/models/volume.dart';

class BookListState {
  const BookListState({
    this.isLoading = false,
    this.errorMessage = '',
    this.items = const [],
    this.query = '',
  });

  final bool isLoading;
  final String errorMessage;
  final List<Volume> items;
  final String query;

  BookListState copyWith({
    bool? isLoading,
    String? errorMessage,
    List<Volume>? items,
    String? query,
  }) {
    return BookListState(
      isLoading: isLoading ?? this.isLoading,
      errorMessage: errorMessage ?? this.errorMessage,
      items: items ?? this.items,
      query: query ?? this.query,
    );
  }
}
