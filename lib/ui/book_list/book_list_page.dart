import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../widgets/search_app_bar.dart';
import 'book_list_cubit.dart';
import 'book_list_item.dart';
import 'book_list_state.dart';

class BookListPage extends StatelessWidget {
  const BookListPage({Key? key}) : super(key: key);

  static const String routeName = 'BookList';

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BookListCubit(),
      child: _ProvidedPage(),
    );
  }
}

class _ProvidedPage extends StatelessWidget {
  _ProvidedPage();

  final _searchAppBarKey = GlobalKey<SearchAppBarState>();

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<BookListCubit>(context);
    final textTheme = Theme.of(context).textTheme;

    return BlocBuilder<BookListCubit, BookListState>(
      builder: (context, state) {
        return Scaffold(
          appBar: SearchAppBar(
            key: _searchAppBarKey,
            onEditingComplete: (query) {
              cubit.search(query);
            },
            title: const Text('Google Books'),
          ),
          body: state.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : state.items.isNotEmpty
                  ? GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                        childAspectRatio: 0.6,
                        crossAxisSpacing: 8,
                        mainAxisSpacing: 8,
                        maxCrossAxisExtent: 120,
                      ),
                      itemCount: state.items.length,
                      itemBuilder: (context, index) =>
                          BookListItem(item: state.items[index]),
                      padding: const EdgeInsets.all(8),
                    )
                  : state.errorMessage.isNotEmpty
                      ? Center(
                          child: Text(state.errorMessage),
                        )
                      : state.query.isEmpty
                          ? Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    'Please start a new search to find the book you are looking for.',
                                    style: textTheme.bodySmall?.copyWith(
                                      color: Colors.grey,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 16.0,
                                  ),
                                  ElevatedButton.icon(
                                    onPressed: () => _searchAppBarKey
                                        .currentState
                                        ?.openSearch(),
                                    icon: const Icon(Icons.search),
                                    label: const Text('Search now'),
                                  ),
                                ],
                              ),
                            )
                          : Center(
                              child: Text(
                                  'No books found searching "${state.query}"'),
                            ),
        );
      },
    );
  }
}
