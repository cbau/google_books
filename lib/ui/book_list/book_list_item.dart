import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../data/models/volume.dart';
import '../book_details/book_details_page.dart';

class BookListItem extends StatelessWidget {
  const BookListItem({
    required this.item,
    Key? key,
  }) : super(key: key);

  final Volume item;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Theme.of(context).primaryColor,
      child: InkWell(
        onTap: () => GoRouter.of(context).pushNamed(
          BookDetailsPage.routeName,
          params: {
            'id': item.id,
          },
          extra: item,
        ),
        child: Column(
          children: [
            Expanded(
              child: item.volumeInfo.imageLinks.smallThumbnail.isEmpty
                  ? const SizedBox()
                  : Hero(
                      tag: 'thumbnail${item.id}',
                      child: CachedNetworkImage(
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.broken_image),
                        fit: BoxFit.cover,
                        imageUrl: item.volumeInfo.imageLinks.smallThumbnail,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => Center(
                          child: CircularProgressIndicator(
                            value: downloadProgress.progress,
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              child: Text(
                '${item.volumeInfo.title}\n',
                maxLines: 2,
                textAlign: TextAlign.center,
                style: Theme.of(context).primaryTextTheme.bodyText2,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
