import 'package:flutter/material.dart';

import 'search_text_field.dart';

class SearchAppBar extends StatefulWidget implements PreferredSizeWidget {
  const SearchAppBar({
    this.actions,
    this.onChanged,
    this.onEditingComplete,
    this.title,
    super.key,
  });

  final List<Widget>? actions;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onEditingComplete;
  final Widget? title;

  @override
  State<SearchAppBar> createState() => SearchAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class SearchAppBarState extends State<SearchAppBar> {
  final _appBarTextFieldTheme = ThemeData(
    brightness: Brightness.dark,
    primarySwatch: const MaterialColor(
      0xFFFFFFFF,
      {
        50: Colors.white,
        100: Colors.white,
        200: Colors.white,
        300: Colors.white,
        400: Colors.white,
        500: Colors.white,
        600: Colors.white,
        700: Colors.white,
        800: Colors.white,
        900: Colors.white,
      },
    ),
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: Colors.white,
    ),
  );
  final _focusNode = FocusNode();

  var _isSearchFieldOpen = false;

  @override
  Widget build(BuildContext context) {
    return _isSearchFieldOpen
        ? AppBar(
            leading: BackButton(
              onPressed: closeSearch,
            ),
            title: Theme(
              data: _appBarTextFieldTheme,
              child: SearchTextField(
                focusNode: _focusNode,
                onChanged: widget.onChanged,
                onEditingComplete: widget.onEditingComplete,
              ),
            ),
          )
        : AppBar(
            actions: [
              IconButton(
                icon: const Icon(Icons.search),
                onPressed: openSearch,
              ),
              ...?widget.actions,
            ],
            title: widget.title,
          );
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void closeSearch() {
    setState(() => _isSearchFieldOpen = false);
    widget.onChanged?.call('');
    widget.onEditingComplete?.call('');
  }

  void openSearch() {
    setState(() => _isSearchFieldOpen = true);
    _focusNode.requestFocus();
  }
}
