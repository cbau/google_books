import 'package:flutter/material.dart';

class SearchTextField extends StatefulWidget {
  const SearchTextField({
    this.focusNode,
    this.onChanged,
    this.onEditingComplete,
    Key? key,
  }) : super(key: key);

  final FocusNode? focusNode;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onEditingComplete;

  @override
  State<SearchTextField> createState() => SearchTextFieldState();
}

class SearchTextFieldState extends State<SearchTextField> {
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      decoration: InputDecoration(
        hintText: 'Search',
        prefixIcon: const Icon(Icons.search),
        suffixIcon: _controller.text.isEmpty
            ? null
            : IconButton(
                onPressed: () {
                  _controller.text = '';
                  setState(() {});
                  widget.onChanged?.call('');
                },
                icon: const Icon(Icons.close),
              ),
      ),
      focusNode: widget.focusNode,
      onChanged: (value) {
        setState(() {});
        widget.onChanged?.call(value);
      },
      onEditingComplete: () => widget.onEditingComplete?.call(_controller.text),
      textInputAction: TextInputAction.search,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
